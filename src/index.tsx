import React from "react";
import ReactDOM from "react-dom";
import "./global.css";

import { BrowserRouter, Routes } from "react-router-dom";

import { Header } from "./components/header";
import { Footer } from "./components/footer";
import { InstitucionalNav } from "./components/institucionalnav";
import { Pages } from "./components/pageswitch";

ReactDOM.render(
  <BrowserRouter>
    <Header />
    <main>
      <div className="institucional-wrapper">
        <div className="page-name-wrapper">
          <a href="#">
            <img src="./images/home-icon.png" alt="Home" />
          </a>
          <img src="./images/arrow-right-icon.png" alt="arrow-right" />
          <span>INSTITUCIONAL</span>
        </div>
        <h1 className="institucional-title">INSTITUCIONAL</h1>
        <div className="institucional-page-wrapper">
          <InstitucionalNav />
          <div className="institucional-item-wrapper">
            <Pages />
          </div>
        </div>
      </div>
    </main>
    <Footer />
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
