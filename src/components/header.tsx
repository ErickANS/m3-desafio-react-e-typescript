import React, { useState } from "react";

import { useWindowScroll } from "react-use";

import { MenuMobile } from "./menumobile";

import "./styles/header.css";

const Header = () => {
  const scrollToTop = () => window.scrollTo({ top: 0, behavior: "smooth" });
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <header className="header">
        <div className="header-top">
          <img
            className="menu-mobile-icon"
            src="/images/menu-mobile-icon.png"
            alt=""
            onClick={() => setIsOpen(true)}
          />
          <MenuMobile isOpen={isOpen} setIsOpen={setIsOpen} />
          <a className="logo" href="#">
            <img
              className="logo-image"
              src="./images/m3-academy-logo.png"
              alt="m3 academy logo"
            />
          </a>
          <div className="search-desktop-wrapper">
            <input
              className="search-input"
              type="text"
              placeholder="Buscar..."
            />
            <img
              className="search-icon"
              src="./images/search-icon.png"
              alt=""
            />
          </div>
          <div className="user-wrapper">
            <a className="user-login" href="#">
              ENTRAR
            </a>
            <img className="cart-icon" src="./images/cart-icon.png" alt="" />
          </div>
        </div>
        <div className="search-mobile-wrapper">
          <input className="search-input" type="text" placeholder="Buscar..." />
          <img className="search-icon" src="./images/search-icon.png" alt="" />
        </div>
        <nav className="header-nav">
          <div className="header-nav-wrapper">
            <a className="header-nav-link" href="#">
              CURSOS
            </a>
            <a className="header-nav-link" href="#">
              SAIBA MAIS
            </a>
          </div>
        </nav>
      </header>
      <div className="fixed-buttons">
        <img
          className="whatsapp-icon"
          src="./images/whatsapp-icon.png"
          alt="whatsapp"
        />
        <div onClick={scrollToTop} className="arrow-scroll-wrapper">
          <img
            className="arrow-scroll-icon"
            src="./images/arrow-up-icon.png"
            alt="seta pra cima"
          />
        </div>
      </div>
    </>
  );
};

export { Header };
