import React from "react";

import { NavLink } from "react-router-dom";

import "./styles/institucionalnav.css";

const InstitucionalNav = () => {
  /*  const pathnames = document.querySelectorAll(".institucional-link");
  const liLinks = document.querySelectorAll(".institucional-links"); */

  /*  const changeActivePage = (i: number) => {
    for (i = 0; i < pathnames.length; i++) {
      if (pathnames[i].getAttribute("href") === window.location.pathname) {
        liLinks[i].classList.add("active");
      }
      if(pathnames[i].getAttribute("href") !== window.location.pathname && Array.fto(liLinks).find())
    }
  }; */

  return (
    <ul className="institucional-links-wrapper">
      <li className="institucional-links">
        <NavLink
          className={(navData) =>
            navData.isActive
              ? "institucional-link-active"
              : "institucional-link"
          }
          to="/"
        >
          Sobre
        </NavLink>
      </li>
      <li className="institucional-links">
        <NavLink
          className={(navData) =>
            navData.isActive
              ? "institucional-link-active"
              : "institucional-link"
          }
          to="/forma-de-pagamento"
        >
          Forma de pagamento
        </NavLink>
      </li>
      <li className="institucional-links">
        <NavLink
          className={(navData) =>
            navData.isActive
              ? "institucional-link-active"
              : "institucional-link"
          }
          to="/entrega"
        >
          Entrega
        </NavLink>
      </li>
      <li className="institucional-links">
        <NavLink
          className={(navData) =>
            navData.isActive
              ? "institucional-link-active"
              : "institucional-link"
          }
          to="/troca-e-devolucao"
        >
          Troca e Devolução
        </NavLink>
      </li>
      <li className="institucional-links">
        <NavLink
          className={(navData) =>
            navData.isActive
              ? "institucional-link-active"
              : "institucional-link"
          }
          to="/seguranca-e-privacidade"
        >
          Segurança e Privacidade
        </NavLink>
      </li>
      <li className="institucional-links">
        <NavLink
          className={(navData) =>
            navData.isActive
              ? "institucional-link-active"
              : "institucional-link"
          }
          to="/contato"
        >
          Contato
        </NavLink>
      </li>
    </ul>
  );
};

export { InstitucionalNav };
