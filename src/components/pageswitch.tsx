import React from "react";

import { Route, Routes } from "react-router-dom";

import { Institucional } from "../pages/institucional";
import { FormaDePagamento } from "../pages/forma-de-pagamento";
import { Entrega } from "../pages/entrega";
import { TrocaEDevolucao } from "../pages/troca-e-devolucao";
import { SegurancaEPrivacidade } from "../pages/seguranca-e-privacidade";
import { Contato } from "../pages/contato";

const Pages = () => {
  return (
    <Routes>
      <Route path="/" element={<Institucional />} />
      <Route path="/forma-de-pagamento" element={<FormaDePagamento />} />
      <Route path="/entrega" element={<Entrega />} />
      <Route path="/troca-e-devolucao" element={<TrocaEDevolucao />} />
      <Route
        path="/seguranca-e-privacidade"
        element={<SegurancaEPrivacidade />}
      />
      <Route path="/contato" element={<Contato />} />
    </Routes>
  );
};

export { Pages };
