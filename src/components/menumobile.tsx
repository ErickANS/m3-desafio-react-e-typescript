import React, { useState } from "react";

import "./styles/menumobile.css";

interface MenuMobileProps {
  isOpen: boolean;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const MenuMobile = ({ isOpen, setIsOpen }: MenuMobileProps) => {
  return (
    <div className={`menu-mobile-wrapper ${isOpen && "menu-open"}`}>
      <div className="menu-mobile-content">
        <div className="menu-mobile-header">
          <span className="menu-span">MENU</span>
          <div className="menu-mobile-header-buttons">
            <span className="menu-mobile-login-button">ENTRAR</span>
            <span
              className="menu-mobile-close-button"
              onClick={() => setIsOpen(false)}
            >
              X
            </span>
          </div>
        </div>
        <div className="menu-mobile-links-wrapper">
          <a
            className="menu-mobile-link"
            href="#"
            onClick={() => setIsOpen(false)}
          >
            CURSOS
          </a>
          <a
            className="menu-mobile-link"
            href="#"
            onClick={() => setIsOpen(false)}
          >
            SAIBA MAIS
          </a>
        </div>
      </div>
      <div
        className={`menu-mobile-overlay ${isOpen && "overlay-open"}`}
        onClick={() => setIsOpen(false)}
      />
    </div>
  );
};

export { MenuMobile };
