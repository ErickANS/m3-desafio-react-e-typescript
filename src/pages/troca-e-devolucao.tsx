import React from "react";

import "../components/styles/institucionalpages.css";

const TrocaEDevolucao = () => {
  return (
    <div className="troca-e-devolucao-wrapper">
      <h1 className="troca-e-devolucao-title">Troca e Devolução</h1>
    </div>
  );
};

export { TrocaEDevolucao };
