import React from "react";

const SegurancaEPrivacidade = () => {
  return (
    <div className="seguranca-e-privacidade-wrapper">
      <h1 className="seguranca-e-privacidade-title">Segurança e Privacidade</h1>
    </div>
  );
};

export { SegurancaEPrivacidade };
