import React from "react";
import { CustomForm } from "../components/customform";

const Contato = () => {
  return (
    <div>
      <CustomForm />
    </div>
  );
};

export { Contato };
